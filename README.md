
# Chirpstack Server Check-in for LoRa Gateway

This repo contains scripts to check and restart failing services on the gateway related to the connection to the LoRa server, Chirpstack.

To use it, call (or better yet, crontab) the SH script. However, you will likely need to install a few python packages and change configuration variables before this works correctly. Please follow the instructions below.

Thus far, we have considered two gateways, a Multitech Conduit running a custom flavor of Linux called *mLinux*, and a raspberry Pi with a LoRa gateway hat, such as the RAK833. There are separate files for each of these, as they linux environments are quite different.

## Install

For starters, this repository should be installed at /opt/lora/ on the device, such that the files within this repository can be reached from the path, /opt/lora/lora-gateway-restart/.  Note that if you are on multitech, then 'git' may not be installed. In that case, run ```sudo opkg update; sudo opkg git```

Clone the repo: ```git clone git@bitbucket.org:ccsg-res/lora-gateway-restart.git /opt/lora/lora-gateway-restart```. It will likely ask you for bitbucket login credentials.

To install for the multitech *or* Pi gateways, run the corresponding install-{platform}.sh script. This will ensure python is present on the system, so it can watch the MQTT stream for status messages to make the gateway is healthy.

Run the following:
```
sudo chmod +x install-multitech.sh
sudo ./install-multitech.sh
```
If you are using multitech; swap 'multitech' for 'pi' if using a Raspberry pi.

### Marshaler Format

Ensure the gateway bridge is using a readable Marshaler format so we can peek into status messages the gateway uplinks over MQTT.

Open the file ```/var/config/chirpstack-gateway-bridge/chirpstack-gateway-bridge.toml``` where the gateway is configured. Navigate down the file until you find ```marshaler=protobuf```. We want the value to read 'json' (without quotes), so change it to that if the variable says protobuf. It should otherwise be 'json'.

Restart the gateway service. On multitech, run ```sudo /etc/init.d/chirpstack-gateway-bridge restart``` or on Pi, run ```sudo systemctl restart chirpstack-gateway-bridge.service```. It should indicate that it Stopped and Started the service.

## Config

### SH script configuration

We need to configure several variables within a configuration file to use this script, especially because certain services are named differently depending on the gateway.

#### The CONF file

There is a configuration file (extension CONF) for multitech and pi gateways. You only need to modify the one for your gateway. We would recommend making a copy of the original one before modifying it. 

Most importantly, you will need the MAC ID/Address of the gateway (a 16 character hexadecimal string) and the address of the server, which we assume to be accessible through an OpenVPN connection.

The {service-name}_SERVICE are the names of the linux services that run in the background to support the gateway's network connection and connection to Chirpstack. 

On Pi, you can view the running services by running ```sudo systemctl```. The packet forwarder service is the most likely one to have a different name.

On the Multitech, the names of services can be found in the /etc/init.d/ directory. There may be two versions of the packet forwarder; if both are running, then you'll need to duplicate a few simple lines of code in the main SH script. To check which one is in use, (ap1 or ap2), look for directories of those names in /var/config/. That directory will also have a file 'global_conf.json', which should contain the gateway_ID near the bottom. You can also check on chirpstack. The IDs should match.

## Checking for Correctness

To run the script, run ``` /opt/lora/lora-gateway-restart/network-diagnostic-multitech.sh```. (or ...-pi.sh if using a RAK-equipped Raspberry Pi) It will take a moment for the gateway to send a status update, which this script will detect. If it did, then you will see that status message and a message saying "All'swell". Otherwise, it will attempt to restart services. 

If something is not installed correctly or a service name is wrong, then you may receive errors. Usually, this indicates that something is wrong in the .conf file. 

To make sure services will restart properly, you can try shutting them off manually and run the main script, and then run a second time to make sure that all is well. It is safest to shut off the packet forwarder or gateway bridge service, to make sure you can stay logged in.

If everything is working, then we will next automate this to run periodically


## Automating the Diagnostic

The most straightforward way to automate things that run periodically on a linux machine is 'crontab'. This lets you establish a schedule by which a command runs on the machine, with granularity to the minute.

Call ```sudo crontab -e``` so we can add an entry. Copy-paste the following onto a new line at the bottom:

For Multitech:
```
*/5 * * * * bash /opt/lora/lora-gateway-restart/network-diagnostic-multitech.sh >> /var/volatile/log/lora-gateway-restart.log 2>&1
```

For Pi:
```
*/5 * * * * bash /opt/lora/lora-gateway-restart/network-diagnostic-pi.sh >> /var/log/lora-gateway-restart.log 2>&1
```

The two are effectively identical, but the multitech variant logs to the volatile logging directory to make sure it's limited storage does not get consumed by log files; note that this is cleared on restart! To check that this is running, you can use ```tail -f -n 25 /var/log/gateway-network-diagnostic.log``` where you should see the output produced by the script (if the file doesn't exist yet, create the file with ```touch```). If that is not working, then syslog should show the calls made by crontab.

If there is an entry for the 'lora-cron.sh' script, delete that entry. This is a more generalized version of that script. Leave in the daily device reset in case other software breaks.

