#!/bin/bash

source /opt/lora/lora-gateway-restart/multitech.conf


if [ `whoami` != root ]; then
    echo "Please run this script as root or using sudo" >&2
    exit
fi

last_checkin=`python3 /opt/lora/lora-gateway-restart/gateway-mqtt-check.py $SERVER_HOST $GATEWAY_ID $TIMEOUT`
echo ""
echo "Uplinked stats at t= $last_checkin"

restart_pkt_forwarder=0
restart_bridge=0
restart_openvpn=0
restart_network=0

if [ -z $last_checkin  ]
then
    echo "Too long since last gateway checkin. Ping the network-server host"
    restart_bridge=1
    restart_pkt_forwarder=1

    ping -c 1 $SERVER_HOST &> /dev/null
    return=$? #just want the return code; 0 indicates reachable, 2 indicates unreachable (latter may vary by platform; 2 is the case on darwin)
    if [ $return -eq 0 ]
    then
        echo 'Successful ping to network-server host' #we can access the network server, but the gateway hasn't shown up correctly. Let's restart the corresponding services
    else
        echo 'Failed to reach network server; check stable address in case the network is down as a whole'
        restart_openvpn=1
        ping -c 1 8.8.8.8 &> /dev/null #ping a common DNS server. If this fails, the network interface is probably down
        return=$? #just want the return code; 0 should give
        if [ $return -eq 0 ]
        then
            echo "Network reachable"
        else
            echo "Network not reachable"
            restart_network=1
        fi
        #look deeper; ping 8.8.8.8 (a common DNS server). If that fails, then restart network interface. If passes, then it's probably just the connection to
    fi
else
    echo "All'swell"
fi

#stop packet forwarder
if [ "$restart_pkt_forwarder" -ne "0" ]
then
    #stop service
    echo "stop packet forwarder"
    $PKT_FORWARDER_SERVICE stop
fi

#stop gateway bridge
if [ "$restart_bridge" -ne "0" ]
then
    #stop service
    echo "stop gateway bridge"
    $GW_BRIDGE_SERVICE stop
fi

#stop openvpn
if [ "$restart_openvpn" -ne "0" ]
then
    #stop service
    echo "stop openvpn"
    $OPEN_VPN_SERVICE stop
fi


#restart network interface
if [ "$restart_network" -ne "0" ]
then
    #stop service
    echo "restart network interface"
    $NETWORK_SERVICE restart
fi


#start openvpn
if [ "$restart_openvpn" -ne "0" ]
then
    #start service
    echo "start openvpn"
    $OPEN_VPN_SERVICE start #Do i need to print anything custom
fi

#start gateway bridge
if [ "$restart_bridge" -ne "0" ]
then
    #start service
    echo "start gateway bridge"
    $GW_BRIDGE_SERVICE start
fi

#start packet forwarder
if [ "$restart_pkt_forwarder" -ne "0" ]
then
    #start service
    echo "start packet forwarder"
    $PKT_FORWARDER_SERVICE start

fi

echo -e "\n"
