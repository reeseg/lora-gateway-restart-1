#/bin/bash

if [ `whoami` != root ]; then
    echo "Please run this script as root or using sudo" >&2
    exit
fi

sudo apt update
sudo apt install python3.8 python3-pip
sudo pip3 install paho-mqtt